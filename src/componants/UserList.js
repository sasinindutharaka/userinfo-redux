import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import {selectUser} from '../actions/userAction';

class UserList extends Component {

    createListItems(){
        return this.props.allusers.map((user) =>{
            return(
                <li key={user.id} onClick={()=>this.props.selectUser(user)}>{user.name} {user.email}</li>
            )
        })
    }


    render() {

        if(!this.props.allusers){
            return(
                <h2>Click the Button</h2>
            )
        }

        return (
            <div>
                <ul>
                {this.createListItems()}
                </ul>
            </div>
        )
    }
}

function mapStateToProps(state){
    return{
        allusers:state.allusers
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({selectUser: selectUser}, dispatch)
}

 export default connect(mapStateToProps,matchDispatchToProps)(UserList);