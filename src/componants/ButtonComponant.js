import React, {Component} from 'react'
import { fetchinfo } from '../actions/userAction'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class ButtonComponant extends Component {
    render(){
    return (
        <div>
            <button onClick= {()=>this.props.fetchinfo()}>Click Here</button>
        </div>
    )
}
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({fetchinfo: fetchinfo}, dispatch)
}

export default connect(null,matchDispatchToProps)(ButtonComponant)
