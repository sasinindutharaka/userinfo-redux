import React from "react";
import ButtonComponant from "./ButtonComponant";
import UserList from "./UserList";
import SelectedUser from './SelectedUser';


function App() {
  return (
    <div className="App">
      <h1>
        Welcome
      </h1>
      <ButtonComponant />
      <br></br>
      <UserList  />
      <br></br>
     <SelectedUser />
    </div>
  );
}

export default App;
