import React, { Component } from 'react'
import { connect } from 'react-redux'

class SelectedUser extends Component {
    render() {
        if(!this.props.selectedUser){
            return(
                <h2>Click user</h2>
            )
        }

        return (
            <div>
                <h2>User ID:{this.props.selectedUser.userId}</h2>
                <h2>User Name:{this.props.selectedUser.name}</h2>
                <h2>User email:{this.props.selectedUser.email}</h2>
                
            </div>
        )
    }
}

function mapStateToProps(state){
    return{
        selectedUser:state.selectedUser
    }
}

export default connect(mapStateToProps)(SelectedUser);