import { combineReducers } from "redux";
import AlluserReducer from "./reducer-Allusers";
import SelectedUserReducer from './reducer-selectedUser';

const rootReducer = combineReducers({
    allusers : AlluserReducer,
    selectedUser: SelectedUserReducer
})

export default rootReducer;