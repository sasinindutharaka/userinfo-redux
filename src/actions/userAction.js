import { FETCH_BUTTON_CLICKED,SELECT_USER } from "./types"

export const fetchinfo =()=>{

    return{
        type: FETCH_BUTTON_CLICKED,
        payload:[
            {
                userId: 1,
                id: 1,
                name:'Sasinindu',
                email:'sasinindu@gmail.com'
            },
            {
                userId: 2,
                id: 2,
                name:'Tharaka',
                email:'tharaka@gmail.com'
            },
            {
                userId: 3,
                id: 3,
                name:'Rathnapriya',
                email:'rathnapriya@gmail.com'
            }
        ]
    }
}

export const selectUser = (user)=>{
    return{
        type:SELECT_USER,
        payload:user
    }

};